## Product overview 

inherits https://docs.google.com/spreadsheets/d/1dqja-DLoG170mX2BsJNRviVzC1zZQa046Vh-bQrk0sY/edit#gid=0  

## User Journey - the 7 steps

1. Lead Generation
1. Warming
1. Viewing
1. Closing
1. On boarding
1. Ten Ex
1. Renewal

## Diagram

Relation  

```mermaid
graph TB;
  j(WhatsApp / Email);

  subgraph frontend
    c1(Dash Living Mobile APP);
    c2(Dashare Mobile APP);
    e1(ARP);
    e2(SAP);
    e(DAP);
    f(Dash.co);
    k(Form Web);
  end

  subgraph backend
    a[(Mongo DB)];
    d(Admin API);
    b(Mobile API);
    g1(HubSpot);
    g2(CloudBeds);
    g3(Salto Pro);
    i(FreshChat);
    l(Prizm Voucher)
  end

  c1-->b-->a;
  f-->e & e2-->d-->a;
  e1-->d
  e2-->j;
  i-->j;
  d-->g1 & g2 & g3 & i;
```

## Terms / Third party services used

- ARP = Available Room Page
- DAP = Dash Admin Panel
- SAP = Suites Admin Panel

- [Metabase](https://www.metabase.com/) - Data analytic tool, features limited? 
- [DataDog](https://www.datadoghq.com/) - Cloud monitoring service / Data analytic tool with rich features 
- [CloudWatch]() - Error log watching
- EasyCity - SG co-living company acquired by Dash 
- [Twilio](https://www.twilio.com/) - Phone calls and text messages (SMS) API service
- Freshchat - Powered by [Freshworks](https://www.freshworks.com/live-chat-software/), messaging and bot web service to integrate with various messaging apps
- AWS - Amazon Web Service
    - S3 - File server?
    - EC2 - ??
    - Cloudfront - CDN to edges
